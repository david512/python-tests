import tkinter as tk


class View:
    # __window = None

    def __init__(self, model):
        self.model = model
        self.init_ui()

    def init_ui(self):
        self.window = tk.Tk()
        self.window.title("App")
        icon = tk.PhotoImage(
            file='/home/david/VSCode/Python/Tests/app/mplayer_icon1.png')
        self.window.tk.call('wm', 'iconphoto', self.window._w, icon)

        self.btn_back = tk.Button(
            self.window, text="Back", command=self.on_btn_back_clicked)
        self.btn_back.grid(column=0, row=0)

        self.btn_up = tk.Button(self.window, text="Up",
                                command=self.on_btn_up_clicked)
        self.btn_up.grid(column=0, row=1)

        self.btn_down = tk.Button(
            self.window, text="Down", command=self.on_btn_down_clicked)
        self.btn_down.grid(column=0, row=2)

#                                                1234567890123456
        self.lbl_1 = tk.Label(self.window, text="2/11      [stop]")
        self.lbl_2 = tk.Label(self.window, text="Dusky - Long Wai")
        self.lbl_3 = tk.Label(self.window, text="[]     3:16/7:36")
        self.lbl_1.config(font=("Mono"))
        self.lbl_2.config(font=("Mono"))
        self.lbl_3.config(font=("Mono"))
        self.lbl_1.grid(column=1, row=0)
        self.lbl_2.grid(column=1, row=1)
        self.lbl_3.grid(column=1, row=2)

        self.window.geometry('316x160')

        self.btn_left = tk.Button(
            self.window, text="Prev", command=self.on_btn_left_clicked)
        self.btn_left.grid(column=0, row=3)

        self.btn_select = tk.Button(
            self.window, text="Play/Pause", command=self.on_btn_select_clicked)
        self.btn_select.grid(column=1, row=3)

        self.btn_right = tk.Button(
            self.window, text="Next", command=self.on_btn_right_clicked)
        self.btn_right.grid(column=3, row=3)

        self.btn_backward = tk.Button(
            self.window, text="<<", command=self.on_btn_backward_clicked)
        self.btn_backward.grid(column=0, row=4)

        self.btn_stop = tk.Button(
            self.window, text="Stop", command=self.on_btn_stop_clicked)
        self.btn_stop.grid(column=1, row=4)

        self.btn_forward = tk.Button(
            self.window, text=">>", command=self.on_btn_forward_clicked)
        self.btn_forward.grid(column=3, row=4)

        self.window.mainloop()

    def on_btn_back_clicked(self):
        # pass
        # self.model.proc.terminate()
        self.model.get_file_name()

    def on_btn_up_clicked(self):
        # pass
        self.model.print_one_line()

    def on_btn_down_clicked(self):
        # pass
        self.model.test()

    def on_btn_left_clicked(self):
        # pass
        self.model.play_prev()

    def on_btn_select_clicked(self):
        self.model.play_pause()

    def on_btn_right_clicked(self):
        pass
        self.model.play_next()

    def on_btn_backward_clicked(self):
        # pass
        self.model.go_backward()

    def on_btn_stop_clicked(self):
        # pass
        self.model.stop()

    def on_btn_forward_clicked(self):
        # pass
        self.model.go_forward()

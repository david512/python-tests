# http://www.mplayerhq.hu/DOCS/tech/slave.txt
# https://pypi.org/project/mplayer.py/
# https://www.linuxquestions.org/questions/linux-software-2/gmplayer-config-msglevel-511503/
import subprocess
# import select
import time


class Model():
    # audio_file = '/home/david/Muzyka/01. House of Cards Main Title Theme.mp3'
    playlist = [
        '/home/david/Muzyka/01. House of Cards Main Title Theme.mp3',
        '/home/david/Muzyka/Fatboy Slim - right here, right now(CDS)/01 - Right Here, Right Now.flac',
        '/home/david/Muzyka/FLC/01. A Love Song.flac',
        '/home/david/Muzyka/FLC/01. Chumbawamba – Tubthumping.flac',
        '/home/david/Muzyka/FLC/02. The River.flac',
        '/home/david/Muzyka/FLC/10 - Ingrid Is a Hybrid.flac',
        '/home/david/Muzyka/FLC/M83 - Go!.flac'
    ]
    playlist_pos = 0
    _mplayer = 0
    _time_length = 0
    # _file_name = ''

    def __init__(self):
        self._mplayer = subprocess.Popen(['mplayer',
                                          '-slave',
                                          '-idle',
                                          '-novideo',
                                          '-really-quiet',
                                          #   '-quiet'
                                          '-msglevel',
                                          'global=4'
                                          ],
                                         stdin=subprocess.PIPE,
                                         stdout=subprocess.PIPE,
                                         stderr=subprocess.STDOUT,
                                         #  bufsize=1
                                         )

        while True:
            line = self.readline()
            # print(line, end='')
            if (line.startswith('connect: No such file or directory')):
                break

        self.load_file(self.playlist[self.playlist_pos])

    def __del__(self):
        self.close_mplayer()

    def close_mplayer(self):
        # self.proc.kill()
        self._mplayer.stdin.close()
        self._mplayer.terminate()
        self._mplayer.wait(timeout=0.5)
        # self.proc.kill()
        # self.t.join()

    # def _readlines(self):
    #     ret = []
    #     while any(select.select([self._mplayer.stdout.fileno()], [], [], 0.6)):
    #         ret.append(self._mplayer.stdout.readline().decode('utf-8'))
    #     return ret

    def command(self, name, *args):
        cmd = '%s%s%s\n' % (name,
                            ' ' if args else '',
                            ' '.join(repr(a) for a in args)
                            )
        self._mplayer.stdin.write(bytes(cmd, 'utf-8'))
        self._mplayer.stdin.flush()

    def readline(self):
        return self._mplayer.stdout.readline().decode('utf-8')

    def command_short_ret(self, name, *args):
        self.command(name, *args)
        return self._mplayer.stdout.readline().decode('utf-8')

    def load_file(self, filepath):
        self.command('loadfile', filepath)
        # while True:
        #     line = self._mplayer.stdout.readline().decode('utf-8')
        #     print(line, end='')
        #     if (line.startswith('connect: No such file or directory')):
        #         # print('\n')
        #         # print(line, end='')
        #         break
        #     if (line.startswith('Playing %s'%(filepath))):
        #         print('\n')
        #         print(line, end='')
        #     if (line.startswith('Starting playback...')):
        #         print(line)
        #         self.read_file_info()
        #         break
        #     if line.startswith('File not found: \'%s\''%(filepath)):
        #         print(line, end='')
        #     if line.startswith('Failed to open %s'%(filepath)):
        #         print(line)
        #         break

    def read_file_info(self):
        print(self.get_property('filename'), end='')
        print(self.get_property('path'), end='')
        print(self.get_property('length'), end='')
        print(self.get_property('audio_format'), end='')
        print(self.get_property('audio_codec'), end='')
        print(self.get_property('audio_bitrate'), end='')
        print(self.get_property('samplerate'), end='')
        print(self.get_property('channels'), end='')
        # print(self.get_property('volume'), end='')

    def get_property(self, property):
        return self.command_short_ret('get_property', property)

    def set_property(self, property, value):
        self.command('set_property', property, value)

    def get_time_pos(self):
        return float(self.get_property('time_pos')[13:])

    def set_time_pos(self, value):
        self.set_property('time_pos', value)

    def play_pause(self):
        self.command('pause')

    def stop(self):
        pass
        # print(self.get_property('pause'))
            # self.command('pause')
            # self.set_time_pos(0)

    def play_prev(self):
        if self.playlist_pos > 0:
            self.playlist_pos -= 1
        else:
            self.playlist_pos = len(self.playlist)-1
        self.load_file(self.playlist[self.playlist_pos])

    def play_next(self):
        if self.playlist_pos < len(self.playlist)-1:
            self.playlist_pos += 1
        else:
            self.playlist_pos = 0
        self.load_file(self.playlist[self.playlist_pos])

    def print_one_line(self):
        line = self.readline()
        print(line, end='')

    def test(self):
        # pass
        print(self.get_time_pos())

    def get_file_name(self):
        # self.command('get_file_name')
        self._file_name = self.command_short_ret('get_file_name')[13:]
        print(self._file_name, end='')

    def go_backward(self):
        self.set_time_pos(self.get_time_pos()-10)

    def go_forward(self):
        self.set_time_pos(self.get_time_pos()+10)

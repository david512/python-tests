#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from model import Model
from view import View

def main():
    model = Model()
    View(model)

if __name__ == '__main__':
    main()
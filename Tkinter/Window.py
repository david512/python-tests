# https://likegeeks.com/python-gui-examples-tkinter-tutorial/

import tkinter as tk

window = tk.Tk()
window.title("Hello Window")

lbl_1 = tk.Label(window, text="line1")
lbl_1.grid(column=0, row=0)

lbl_2 = tk.Label(window, text="line2")
lbl_2.grid(column=0, row=1)

lbl_3 = tk.Label(window, text="line3")
lbl_3.grid(column=0, row=2)

window.geometry('320x100')

def onPrevClicked():
    pass

def onPlayClicked():
    pass

def onPauseClicked():
    pass

def onStopClicked():
    pass

def onNextClicked():
    pass


btn_prev = tk.Button(window, text="Prev", command=onPrevClicked) #, bg="orange", fg="red")
btn_prev.grid(column=0, row=3)

btn_play = tk.Button(window, text="Play", command=onPlayClicked) #, bg="orange", fg="red")
btn_play.grid(column=1, row=3)

btn_pause = tk.Button(window, text="Pause", command=onPauseClicked) #, bg="orange", fg="red")
btn_pause.grid(column=2, row=3)

btn_stop = tk.Button(window, text="Stop", command=onStopClicked) #, bg="orange", fg="red")
btn_stop.grid(column=3, row=3)

btn_next = tk.Button(window, text="Next", command=onNextClicked) #, bg="orange", fg="red")
btn_next.grid(column=4, row=3)

window.mainloop()